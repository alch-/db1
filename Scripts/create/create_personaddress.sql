CREATE TABLE PersonAddress(
    PersonID    char(5) CONSTRAINT key5 PRIMARY KEY,
    AddressID   char(5) REFERENCES Address(AddressID)
);
