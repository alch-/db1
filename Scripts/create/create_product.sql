CREATE TABLE Product(
    ProductID   char(5) CONSTRAINT key7 PRIMARY KEY,
    Name        char(40),
    SafetyStockLevel    char(40),
    ReorderPoint    char(40),
    StandardCost    char(40),
    ListPrice       char(40),
    ProductCategoryID   char(5) REFERENCES ProductCategory(ProductCategoryID)
);
