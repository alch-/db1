CREATE TABLE SalesOrderHeader(
    SalesOrderID    char(10) CONSTRAINT key9 PRIMARY KEY,
    OrderDate       date,
    DueDate         date,
    OnlineOrderFlag date,
    CustomerID      char(10) REFERENCES Customer(CustomerID),
    CreditCardID    char(10) REFERENCES CreditCard(CreditCardID),
    SubTotal        numeric(20),
    TaxAmt          numeric(20),
    Freight         numeric(20),
    TotalDue        numeric(20)
);
