CREATE TABLE ProductCategory(
    ProductCategoryID   char(5) CONSTRAINT key6 PRIMARY KEY,
    Name                char(40)
);
