CREATE  TABLE Customer(
    CustomerID char(5) CONSTRAINT key3 PRIMARY KEY,
    PersonID    char(5) REFERENCES Person(PersonID)
);
