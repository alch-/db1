CREATE TABLE Address(
    AddressID   char(5) CONSTRAINT key1 PRIMARY KEY,
    AddressLine1    char(40),
    AddressLine2    char(40),
    City            char(40),
    PostalCode      char(40)
);
