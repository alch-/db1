CREATE TABLE Person(
    PersonID    char(5) CONSTRAINT key4 PRIMARY KEY,
    Title       char(40),
    FirstName   char(40),
    LastName    char(40)
);
