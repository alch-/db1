CREATE TABLE SalesOrderDetail(
    SalesOrderID    char(10) CONSTRAINT key8 PRIMARY KEY,
    SalesOrderDetailID  char(10) REFERENCES SalesOrderHeader(SalesOrderID),
    OrderQty            char(10),
    ProductID           char(5) REFERENCES Product(ProductID),
    UnitPrice           numeric(15),
    UnitPriceDiscount   numeric(15),
    LineTotal           numeric(20)
);
