CREATE TABLE CreditCard(
    CreditCardID    char(5) CONSTRAINT key2 PRIMARY KEY,
    CardType        char(40),
    CardNumber      char(40),
    ExpMonth        char(40),
    ExpYear         char(40)
);
